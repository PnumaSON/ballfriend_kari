#define GLOBAL_INSTANCE 
#include "GV.h"

//ループで必ず行う３大処理
int ProcessLoop(){
	if (ProcessMessage() != 0)return -1;//プロセス処理がエラーなら-1を返す
	if (ClearDrawScreen() != 0)return -1;//裏画面クリア処理がエラーなら-1を返す
	SetDrawScreen(InterFaceHandle);//インターフェース出力をかませる
	if (ClearDrawScreen() != 0)return -1;//インタフェース画面クリア処理がエラーなら-1を返す

	GetHitKeyStateAll_2();//現在のキー入力処理を行う
	GetHitPadStateAll();  //現在のパッド入力処理を行う
	GetMousePoint(&MouseX, &MouseY);//マウスの位置を取得する
	GetMouseState();//マウスの入力処理を行なう
    return 0;
}

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpCmdLine,int nCmdShow){
    ChangeWindowMode(TRUE);//ウィンドウモード
	SetMainWindowText( "ボールフレンド(仮)" ) ;
	SetOutApplicationLogValidFlag( FALSE ) ;

    if(DxLib_Init() == -1 || SetDrawScreen( DX_SCREEN_BACK )!=0) return -1;//初期化と裏画面化
	InterFaceHandle = MakeScreen(640, 480, TRUE);//一時出力用ハンドル　これを加工して最終出力とする

    while(ProcessLoop()==0){//メインループ
        switch(func_state){
            case 0:
				firstini();
				load();
				func_state=90;
                break;
            case 90:
				DrawExtendGraph(0, 0, 640, 480, img_board[0], FALSE);
				if (CheckStateMouse(MOUSELEFT) == 1){
					func_state = 100;
				}
                break;
			case 100://パートナー選択
				select();
				break;
			case 101:
				ini();
				func_state = 102;
				break;
			case 102:
				menu();
				break;
			case 103:
				battle();
				break;
			case 104:
				date();
				break;
			case 105:
				end();
				break;
            default:
                printfDx("不明なfunc_state\n");
                break;
        }
		//DrawFormatString(400, 0, color[0], "X = %d Y = %d", MouseX, MouseY);
		if (CheckStateKey(KEY_INPUT_ESCAPE) == 1)break;//エスケープが入力されたらブレイク
		SetDrawMode(DX_DRAWMODE_BILINEAR);
		SetDrawScreen(DX_SCREEN_BACK);
		DrawExtendGraph(0, 0, windowratio_x, windowratio_y, InterFaceHandle, TRUE);
		ScreenFlip();//裏画面反映
		count++;
    }

    DxLib_End();//ＤＸライブラリ終了処理
    return 0;
}