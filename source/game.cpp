#include "GV.h"


void select(){
	static int k = 100;
	DrawExtendGraph(0, 0, 640, 480, img_board[1], FALSE);
	DrawExtendGraph(50, 50, 205, 300, img_card[1], FALSE);
	DrawExtendGraph(245, 50, 400, 300, img_card[0], FALSE);
	DrawExtendGraph(450, 50, 605, 300, img_card[2], FALSE);

	DrawExtendGraph(50, 320, 590, 380, img_board[2], TRUE);
	if (CheckStateMouse(MOUSELEFT) == 1){
		if (MouseSquare(245, 50, 400, 300)==1){
			func_state = 101;
		}

		if (MouseSquare(50, 50, 205, 300) == 1 || MouseSquare(450, 50, 605, 300) == 1){
			k = 0;
			PlaySoundMem(sound_se[0], DX_PLAYTYPE_BACK);
		}
	}
	if (k < 50){
		DrawExtendGraph(50, 370, 590, 430, img_board[3], TRUE);
	}
	k++;
}


void menu(){
	DrawExtendGraph(0, 0, 640, 480, img_board[4], FALSE);

	//day
	DrawFormatString(470, 0, color[3], " %d日目", day);

	if (battleflag == 0){
		DrawExtendGraph(50, 350, 205, 450, img_board[5], TRUE);
	}
	else{
		SetDrawBright(100, 100, 100);
		DrawExtendGraph(50, 350, 205, 450, img_board[5], TRUE);
		SetDrawBright(255,255, 255);
	}
	if (dateflag == 0){
		DrawExtendGraph(245, 350, 400, 450, img_board[6], TRUE);
	}
	else{
		SetDrawBright(100, 100, 100);
		DrawExtendGraph(245, 350, 400, 450, img_board[6], TRUE);
		SetDrawBright(255, 255, 255);
	}

	DrawExtendGraph(450, 350, 605, 450, img_board[7], TRUE);


	if (CheckStateMouse(MOUSELEFT) == 1){
		if (MouseSquare(50, 350, 205, 450) == 1 && battleflag == 0){
			battleflag = 1;
			func_state = 103;
		}
		else if (MouseSquare(245, 350, 400, 450) == 1 && dateflag == 0){
			dateflag = 1; 
			func_state = 104;
		}
		else if (MouseSquare(450, 350, 605, 450) == 1){
			func_state = 101;
		}

	}
}


void myturn(){
	//Damage


}

void eneturn(){

}
void battle(){
	static int turn = 0, flag = 0, damage = 0;
	static int NHP = HP, NEHP = EHP;


	DrawExtendGraph(0, 0, 640, 480, img_board[1], FALSE);
	DrawExtendGraph(65, 150, 200, 380, img_card[0], FALSE);
	DrawExtendGraph(450, 50, 550, 250, img_card[2], FALSE);
	SetFontSize(16);
	DrawFormatString(65, 390, color[1], "HP %d PW %d",NHP,PW);
	DrawFormatString(450, 260, color[1], "HP %d PW %d",NEHP,EPW);
	SetFontSize(48);
	if (turn == 0){
		DrawFormatString(40, 420, color[1], "バスケットボールが現れた!" );
		NHP = HP, NEHP = EHP;
		turn = 0, flag = 0, damage = 0;
		if (CheckStateMouse(MOUSELEFT) == 1){
			turn++;
		}
	}
	else if (NHP <= 0){
		DrawFormatString(40, 420, color[1], "負けてしまった");
		if (CheckStateMouse(MOUSELEFT) == 1){
			turn = 0;
			func_state = 102;
		}
	}
	else if (NEHP <= 0){
		DrawFormatString(40, 420, color[1], "勝利した！");
		if (CheckStateMouse(MOUSELEFT) == 1){
			turn = 0;
			func_state = 105;
		}
	}
	else{
		if (turn % 2 == 1){
			if (damage == 0){
				if (PW / 10 != 0){
					damage = PW + rand() % (PW / 10);
				}
				else{
					damage = PW;
				}
				//NEHP -= damage;
			}
			DrawFormatString(40, 420, color[1], "ボールは友達！ %dダメージ",damage);
			if (CheckStateMouse(MOUSELEFT) == 1){
				NEHP -= damage;
				damage = 0;
				turn++;
			}
		}
		else{
			if (damage == 0){
				damage = EPW + rand() % (EPW / 10);
				//NHP -= damage;
			}
			SetFontSize(24);
			DrawFormatString(40, 420, color[1], "先生、バスケがしたいです！ %dダメージ", damage);
			SetFontSize(48);
			if (CheckStateMouse(MOUSELEFT) == 1){
				NHP -= damage;
				damage = 0;
				turn++;
			}
		}


	}

}

void date(){
	static int Start = 0, nadec = 0, nadecount = 0;
	static int kakoX, kakoY;
	int Time;
	// スタートタイムを取得
	if (Start == 0){
		Start = GetNowCount();
	}
	DrawExtendGraph(0, 0, 640, 480, img_board[1], FALSE);



	Time = (GetNowCount() - Start) / 1000;

	//TimeOut
	if (10 - Time <= 0){
		DrawFormatString(50, 50, color[1], "撫でろ！");
		DrawFormatString(400, 0, color[1], " 残り0秒");
		DrawFormatString(400, 50, color[1], " %dなで", nadecount);
		DrawExtendGraph(150, 100, 490, 440, img_board[8], TRUE);

		DrawFormatString(50, 350, color[1], " HPが%d PWが%d上がった！", nadecount / 10, nadecount / 30);
		DrawFormatString(60, 400, color[1], " 今のHP %d PW %d", HP+nadecount / 10, PW+nadecount / 30);
		if (CheckStateMouse(MOUSELEFT) == 1){
			HP += nadecount / 10;
			PW += nadecount / 30;
			func_state = 102;
			Start = 0, nadec = 0, nadecount = 0;
		}

	}
	else{
		DrawFormatString(50, 50, color[1], "撫でろ！");
		DrawFormatString(400, 0, color[1], " 残り%d秒", 10 - Time);
		DrawFormatString(400, 50, color[1], " %dなで", nadecount);
		//ball
		if (CheckStateMouse(MOUSELEFT) >= 1){
			if (MouseSquare(150, 100, 490, 440) == 1){
				if ((MouseX - kakoX)*(MouseX - kakoX) + (MouseY - kakoY)*(MouseY - kakoY) > 255){
					nadecount++;
					nadec = 10;
				}
			}
		}
		//ball
		if (0 < nadec){
			int x1 = rand() % 10;
			int y1 = rand() % 10;
			int x2 = rand() % 10;
			int y2 = rand() % 10;
			DrawExtendGraph(150 + x1 - 5, 100 + y1 - 5, 490 + x2 - 5, 440 + y2 - 5, img_board[8], TRUE);
			DrawExtendGraph(400 + x1 - 5, 50 + y1 - 5, 600 + x2 - 5, 250 + y2 - 5, img_board[9], TRUE);
		}
		else{
			DrawExtendGraph(150, 100, 490, 440, img_board[8], TRUE);
		}
		//hand
		DrawGraph(MouseX - 64, MouseY - 64, img_board[10], TRUE);

		if (0 < nadec){
			nadec--;
		}
		kakoX = MouseX;
		kakoY = MouseY;

	}
}

